import { Component, OnInit, ViewChild } from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog'
import {MatDialogModule} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { max } from 'rxjs/operators';


export interface Comments {
  username: string;
  body: string;
  title: string;
}
export interface Commentsdetail {
  user: string;
  body: string;
}

@Component({
  selector: 'app-detailcomment',
  templateUrl: './detailcomment.component.html',
  styleUrls: ['./detailcomment.component.scss']
})

export class DetailcommentComponent implements OnInit {
  datacomment:any=[]
  displayedColumns: string[] = ['username', 'title'];
  
  displayedColumndetail: string[] = ['user', 'body'];
  dataSource = new MatTableDataSource<Comments>(this.datacomment);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  username: string ="";
  password: string = "";
  userid: string ="";
  data:any=[]
  userset:any=[]
  comdata:any=[]
  comdata2:any=[]
  datacom:any=[]
  constructor(private router: Router,
    private actRoute: ActivatedRoute,
    private httpClient:HttpClient,) {
      
      this.httpClient.get(`https://jsonplaceholder.typicode.com/users/`).subscribe((data) => 
      {  
        this.userset = data
      })
     }


  ngOnInit() {
    
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.userid =  localStorage.getItem('id') || ''
    this.username =  localStorage.getItem('username') || '' 
    

    this.httpClient.get("https://jsonplaceholder.typicode.com/posts/"+id).subscribe((item) => 
      {  
        this.data = item
        for(let user of this.userset){
              if(this.data.userId == user.id){
                this.httpClient.get("https://jsonplaceholder.typicode.com/posts/"+id+"/comments").subscribe((item) => 
                { 
                  this.comdata2 = item

                  console.log('',this.comdata2)
                  this.comdata = new MatTableDataSource<Commentsdetail>(this.comdata2);
                
                  this.datacomment.push({
                    username :user.username,
                    title : this.data.title,
                    body : this.data.body,
                    count : this.comdata2.length,
                    userId : user.id,
                    postId : this.data.id,
                    // comment: this.comdata
                  })  
                  this.dataSource = new MatTableDataSource<Comments>(this.datacomment);
                    
                  })
                
              }
              
        }
      })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  }
