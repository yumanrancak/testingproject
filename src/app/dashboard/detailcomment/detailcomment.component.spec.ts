import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailcommentComponent } from './detailcomment.component';

describe('DetailcommentComponent', () => {
  let component: DetailcommentComponent;
  let fixture: ComponentFixture<DetailcommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailcommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailcommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
