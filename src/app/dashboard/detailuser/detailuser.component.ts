import { Component, OnInit, ViewChild } from '@angular/core';

import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog'
import {MatDialogModule} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { max } from 'rxjs/operators';


export interface Comments {
  username: string;
  body: string;
}

@Component({
  selector: 'app-detailuser',
  templateUrl: './detailuser.component.html',
  styleUrls: ['./detailuser.component.scss']
})

export class DetailuserComponent implements OnInit {
  datacomment:any=[]
  displayedColumns: string[] = ['username','titik', 'title'];
  dataSource = new MatTableDataSource<Comments>(this.datacomment);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  username: string ="";
  password: string = "";
  userid: string ="";
  data:any=[]
  userset:any=[]
  comdata:any=[]
  comdata2:any=[]
  datacom:any=[]
  constructor(private router: Router,
    private httpClient:HttpClient,) {
      
      this.httpClient.get(`https://jsonplaceholder.typicode.com/users/`).subscribe((data) => 
      {  
        this.userset = data
      })
     }


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.userid =  localStorage.getItem('id') || ''
    this.username =  localStorage.getItem('username') || '' 
    this.datacomment.push({
      username:this.username,
      name:localStorage.getItem('name') || '' ,
      email:localStorage.getItem('email') || '' ,
      address:localStorage.getItem('addres') || '' ,
      phone:localStorage.getItem('phone') || '' ,
    })
    
                  
    this.dataSource = new MatTableDataSource<Comments>(this.datacomment);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  }
