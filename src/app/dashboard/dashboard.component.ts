import { Component, OnInit, ViewChild,EventEmitter } from '@angular/core';

import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog'
import {MatDialogModule} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { max } from 'rxjs/operators';


export interface Comments {
  username: string;
  body: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  datacomment:any=[]
  displayedColumns: string[] = ['username', 'title'];
  dataSource = new MatTableDataSource<Comments>(this.datacomment);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  username: string ="";
  password: string = "";
  userid: string ="";
  data:any=[]
  userset:any=[]
  comdata:any=[]
  comdata2:any=[]
  datacom:any=[]
  constructor(private router: Router,
    private httpClient:HttpClient,) {
      
      this.httpClient.get(`https://jsonplaceholder.typicode.com/users/`).subscribe((data) => 
      {  
        this.userset = data
      })
     }


  ngOnInit() {
    
    this.userid =  localStorage.getItem('id') || ''
    this.username =  localStorage.getItem('username') || '' 

    this.httpClient.get(`https://jsonplaceholder.typicode.com/posts/`).subscribe((item) => 
      {  
        this.data = item
        var i = 2 
        for(let user of this.userset){
            for(let comment of this.data){
              
              if(comment.userId == user.id){
                this.httpClient.get("https://jsonplaceholder.typicode.com/posts/"+comment.id+"/comments").subscribe((item) => 
                { 
                  this.comdata = item
                  this.datacomment.push({
                    username :user.username,
                    title : comment.title,
                    count : this.comdata.length,
                    userId : user.id,
                    postId : comment.id
                  })

                  
                  this.dataSource = new MatTableDataSource<Comments>(this.datacomment);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  this.dataSource.filterPredicate = (data, filter) => {
                    return this.displayedColumns.some(ele => {
                      return ele != 'action' && data[ele] && data[ele].toLowerCase().indexOf(filter) !== -1;
                    });
                  };
                })  
                
              }
              
            }
            
          i=0
        }
      })
  }
  // applyFilter(filterValue: any) {
  //   let test =filterValue.key as string
  //   console.log('test',test)
  //   this.dataSource.filter = test.trim().toLowerCase()

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value.toLowerCase();
      console.log(filterValue); 
     
    this.dataSource.filter = filterValue.trim();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  }
