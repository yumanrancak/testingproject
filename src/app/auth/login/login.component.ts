import { Component, OnInit } from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
// import {MatDialogModule} from '@angular/material/dialog';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  public loginInvalid = false;
  private formSubmitAttempt = false;
  loading = false;
  userset:any=[];
  datas:any=[];
  submitted = false;
  users :string = "Bret"
  
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private httpClient:HttpClient,
    private router: Router,
    // private authService: AuthenticationService,
    private userserv: UserService
  ) {
  
    this.form = this.fb.group({
      username: ['',Validators.required],
      password: ['', Validators.required]
    });
  }
  get f() { return this.form.controls; }

  async ngOnInit(): Promise<void> {

  // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  
  onSubmit() {
    this.loginInvalid = false
    this.formSubmitAttempt = false
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      this.loginInvalid = true
      return;
    }
    else{
      localStorage.setItem("username", this.f.username.value);
      localStorage.setItem("password", this.f.password.value);
      var username = this.f.username.value
      var password = this.f.password.value
      
      this.loading = true;
      this.httpClient.get(`https://jsonplaceholder.typicode.com/users/`).subscribe((data) => 
      {  
        this.userset = data
        
        for(let user of this.userset){
          if(user.username == username && user.username == password){
            
            this.loginInvalid = false
            this.datas  =  user
            localStorage.setItem("id", user.id);
            localStorage.setItem("email", user.email);
            localStorage.setItem("phone", user.phone);
            localStorage.setItem("name", user.name);
            localStorage.setItem("addres", user.address.street +', '+ user.address.suite +', '+ user.address.city +', '+ user.address.zipcode);
            console.log('masuk',this.datas)
            Swal.fire('Good Well', 'Succesfully Log In', 'success')              
            this.router.navigateByUrl('/dashboard');
            break;
            
          }        
          else{
            console.log('gagal','tes')
            this.loginInvalid = true
          }
        }
      })
    }
    
  }
    
}