import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './auth/homepage/homepage.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetailcommentComponent } from './dashboard/detailcomment/detailcomment.component';
import { DetailuserComponent } from './dashboard/detailuser/detailuser.component';

const routes: Routes = [
    {
      path:'login',
      component:LoginComponent
    },
    {
      path:'homepage',
      component:HomepageComponent
    },
    {
      path:'dashboard',
      component:DashboardComponent,
    },
    {
      path: 'detailuser/:id',
      component: DetailuserComponent,
    },
    {
      path: 'detailcomment/:id',
      component: DetailcommentComponent,
    },
    // {
    //   path:'admin',
    //   loadChildren: ()=>import('./admin/admin.module').then(mod=>mod.AdminModule),
    //   data: { preload: true }
    // },
    {
      path:'',
      redirectTo:'/homepage',
      pathMatch:'full'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
