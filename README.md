
# Homepage 

![Homepage 19-12-2021](/src/assets/hp.png)

-> Pada Homepage hanya tampilan awal yang tersedia button login

# Login 

![Login 19-12-2021](/src/assets/login.png)

-> Pada saat login diwajibkan mengisi username & password
-> akan terjadi error jika username / password tidak diisi dan bilan username / password tidak ada dalam data.

# Dashboard 

![Dashboard 19-12-2021](/src/assets/dashboard.png)

-> Tampilan awal pada saat berhasil login
-> Menampilkan data posting seluruh user

# Detail Comments 

![Detail Comments 19-12-2021](/src/assets/detailcomments.png)

-> Menampilkan data detail posting dari user yang dipilih

# Detail Profile 

![DetailProfile 19-12-2021](/src/assets/detailprofile.png)

-> Menampilkan data detail profile user login


# MyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
